<!DOCTYPE html>
<html>
    <head>
        <title>Henley Direct - @yield('title')</title>

        <link href="{{ URL::asset('semantic/semantic/dist/semantic.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="{{ URL::asset('semantic/semantic/dist/semantic.min.js') }}"></script>
        <script src="{{ URL::asset('js/general.js') }}"></script>
        
    </head>
    <body>
        <div class="container">
            
            <div class="ui inverted vertical masthead center aligned segment">
            
                <div class="ui container">
                    
                    @if(Auth::check())
                        @include('_nav_auth')
                    @else
                        @include('_nav')
                    @endif

                    @yield('header_content')

                </div>
            </div>
            
            <div class="ui vertical stripe segment">
                @yield('content')
            </div>
            
        </div>
       
        <div class="ui coupled modal">
            @include('modals._login')
            @include('modals._register')
            @include('modals._verification')
            @yield('modal');
        </div>
        
        @yield('script')
        
    </body>
</html>
