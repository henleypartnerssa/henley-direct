<div class="ui menu">
    
    <div class="header item">Henley Direct</div>
    
    <a class="item @yield('l1')" href="{{ url('/') }}">Home</a>
    <a class="item @yield('l2')" href="{{ url('/programs') }}"> Programs</a>
    <a class="item @yield('l3')" href="{{ url('/locations') }}"> Locations</a>

    <div class="right menu">
        
        <div class="item">
            <div class="ui primary signup button">Sign up</div>
        </div>
        <div class="item">
            <div class="ui primary login button">Log-in</div>
        </div>
        
        <div class="item">
            <div class="ui floating dropdown labeled search icon button">
                <i class="world icon"></i>
                <span class="text">Language</span>
                <div class="menu">
                    <div class="item">Arabic</div>
                    <div class="item">Chinese</div>
                    <div class="item">Danish</div>
                    <div class="item">Dutch</div>
                    <div class="item">English</div>
                    <div class="item">French</div>
                    <div class="item">German</div>
                    <div class="item">Greek</div>
                    <div class="item">Hungarian</div>
                    <div class="item">Italian</div>
                    <div class="item">Japanese</div>
                    <div class="item">Korean</div>
                    <div class="item">Lithuanian</div>
                    <div class="item">Persian</div>
                    <div class="item">Polish</div>
                    <div class="item">Portuguese</div>
                    <div class="item">Russian</div>
                    <div class="item">Spanish</div>
                    <div class="item">Swedish</div>
                    <div class="item">Turkish</div>
                    <div class="item">Vietnamese</div>
                </div>
            </div>
        </div>
                
    </div>
    
</div>