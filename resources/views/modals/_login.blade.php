<div class="ui login modal transition hidden">
    
    <i class="close icon"></i>
    
    <div class="header">Login Form</div>
    
    <div class="ui middle aligned center aligned grid">

        <div class="ui container">

            <div class="ui login large form">
                
                <div class="ui stacked segment">
                    
                    <input type="hidden" name="_token" id="login_token" value="{{ csrf_token() }}" />
                    
                    <div class="required field">
                        <label>Username</label>
                        {!! Form::text('username',null , ['id' => 'username', 'placeholder' => 'Your Email Address...']) !!}
                    </div>
                    
                    <div class="required field">
                        <label>Password</label>
                        <input type="password" id="login_password" placeholder="Your Password..." />
                    </div>
                    
                    <div class="ui logon fluid large blue submit button">Log On</div>
                    
                    <div class="ui login negative hidden message">
                        
                        <div class="header"></div>
                        <p></p>
                        
                    </div>
                    
                </div>
                

            </div>

        </div>

    </div>
    
</div>