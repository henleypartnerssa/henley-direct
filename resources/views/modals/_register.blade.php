<div class="ui first coupled modal transition hidden">
    
    <i class="close icon"></i>
    
    <div class="header"> Registration Form</div>
    
    <div class="ui middle aligned center aligned grid">

        <div class="ui container">

            <div class="ui register large form">
                
                <div class="ui stacked segment">
                    
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
                    
                    <div class="two fields">
                        
                        <div class="required field">
                          <label>First Name</label>
                          {!! Form::text('name',null , ['id' => 'name']) !!}
                        </div>
                        <div class="required field">
                          <label>Last Name</label>
                          {!! Form::text('lastname',null , ['id' => 'lastname']) !!}
                        </div>

                    </div>

                    <div class="two fields">

                        <div class="required field">
                          <label>Password</label>
                          <input type="password" name="register_password" id="register_password">
                        </div>

                        <div class="required field">
                          <label>Re-type Password</label>
                          <input type="password" name="retypePassword" id="retypePassword">
                        </div>

                    </div>

                    <div class="two fields">

                        <div class="required field">
                          <label>Email Address</label>
                          {!! Form::email('email',null , ['id' => 'email']) !!}
                        </div>

                        <div class="required field">
                          <label>Re-type Email Address</label>
                          {!! Form::email('retypeEmail',null , ['id' => 'retypeEmail']) !!}
                        </div>

                    </div>
                    
                    <div class="ui register fluid large blue submit button">Register</div>

                </div>
                

            </div>

        </div>

    </div>
    
</div>