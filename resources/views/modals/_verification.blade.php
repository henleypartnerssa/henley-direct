<div class="ui second coupled modal transition hidden">
            
    <i class="close icon"></i>
    <div class="header">
      Thank You for Registering
    </div>
    <div class="content">  
        <div class="description">
            <p>We have sent a verification email to the following email address: <i><b><span id='echoEmail'>support@henleyglobal.com</span></b></i></p>
            <p>Please click on the verification link to complete the registration process.</p>
        </div>
    </div>
    
    <div class="actions">
      <div class="ui blue close button">OK</div>
    </div>
    
</div>