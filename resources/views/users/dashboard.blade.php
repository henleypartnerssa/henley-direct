@extends('app')

@section('title', 'Pin')

@section('content')
    <div class="ui text container">

        <div class="ui segment">
            
            <h1>Dashboard</h1>
            
            <p>Center</p>
            
            <div class="ui left rail">
                
                <div class="ui large vertical menu">
                    <a class="item active">
                      Home
                    </a>
                    <a class="item">
                      Messages
                    </a>
                    <a class="item">
                      Friends
                    </a>
              </div>
                
            </div>
            
            <div class="ui right rail">
                <div class="ui segment">    
                <div class="ui vertical small steps">
                <div class="completed step">
                  <i class="truck icon"></i>
                  <div class="content">
                    <div class="title">Shipping</div>
                    <div class="description">Choose your shipping options</div>
                  </div>
                </div>
                <div class="completed step">
                  <i class="credit card icon"></i>
                  <div class="content">
                    <div class="title">Billing</div>
                    <div class="description">Enter billing information</div>
                  </div>
                </div>
                <div class="active step">
                  <i class="info icon"></i>
                  <div class="content">
                    <div class="title">Confirm Order</div>
                    <div class="description">Verify order details</div>
                  </div>
                </div>
              </div>
                
            </div>
                </div>
            
        </div>
        
    </div>
@stop