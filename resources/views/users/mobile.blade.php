@extends('app')

@section('title', 'Verified Account')

@section('content')
    <div class="ui text container">

        <h1 class="ui header">User Account Verification</h1>
        <p>
           Account Verification complete. Due to the Confidentiality of the the information you will
           be submitting for your application we would like to send you a one-time pin to your cell phone.
           Please fill in your mobile phone number below:
        </p>
        
        <div class="ui large form">
            
            <div class="inline fields">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
                <div class="four wide field">
                    <label>Mobile Number</label>
                    <input type="text" name="code" id="code" placeholder="code">
                </div>
                
                <div class="eight wide field">
                    <input type="text" name="number" id="number" placeholder="number...">
                </div>
                
                <div class="field">
                    <div id="sendButton" class="ui fluid blue submit button">send pin</div>
                </div>
                
            </div>
            
        </div>
        
    </div>
@stop

@section('script')
    <script src="{{ URL::asset('js/users_mobile.js') }}"></script>
@stop