@extends('app')

@section('title', 'Account Locked')

@section('content')
    <div class="ui text container">

        <h1 class="ui header">Account Locked</h1>
        <p>
           Your account has been locked. Please contact support: <b>support@henley-direct.com</b>
        </p>
        
    </div>
@stop

@section('script')
    <script src="{{ URL::asset('js/general.js') }}"></script>
@stop