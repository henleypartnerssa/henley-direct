@extends('app')

@section('title', 'Pin')

@section('content')
    <div class="ui text container">

        <h1 class="ui header">Enter Your Pin</h1>
        <p>
           We have sent you a 4-digit varfication pin number to the following mobile number: <b>{{ Auth::user()->cell_no }}</b>
        </p>
        
        <div class="ui pin large form">
            
            <div class="inline fields">
                
                <div class="three wide field">
                    <label>Pin:</label> 
                    <input type="text" name="num1" id="num1" maxlength="1">
                </div>
                <div class="two wide field">
                    <input type="text" name="num2" id="num2" maxlength="1">
                </div>

                <div class="two wide field">
                    <input type="text" name="num3" id="num3" maxlength="1">
                </div>

                <div class="two wide field">
                    <input type="text" name="num4" id="num4" maxlength="1">
                </div>
                
            </div>
            <div class="inline fields">
                
                <div class="field">
                    <button id="checkPin" class="ui fluid blue submit button">Send</button>
                </div>
                <div class="field">
                    <a class="ui fluid blue submit button" href="/">Change Mobile Number</a>
                </div>
                
            </div>
           
           <div class="ui pin negative hidden message">
                        
                <div class="header"></div>
                <p></p>
                
            </div>
           
        </div>
        
    </div>
@stop


@section('script')
    <script src="{{ URL::asset('js/users_pin.js') }}"></script>
@stop