<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*************************
           AUTH
/*************************/
Route::controllers(['auth' => 'Auth\AuthController']);

/*************************
      PUBLIC PAGES
/*************************/
Route::get('/', function () { return view('pages.welcome'); }); // Home Page
Route::get('/programs', function () { return view('pages.programs'); });
Route::get('/locations', function () { return view('pages.locations'); });

/*************************
      USERS POST
/*************************/
Route::post('/users/register', 'UsersController@register');         // Recieves data POST Requests for Registration and Reponds 1/0
Route::post('/users/login', 'UsersController@login');               // Recieves data POST Requests for Login and Responds 1/0
Route::post('/users/mobile/number', 'UsersController@sendPin');     // Recieves data POST Requests to update Users mobile number
Route::post('/users/mobile/pin', 'UsersController@checkPin');       // Recieves data POST Requests to valide Pin

/*************************
      USERS GET
/*************************/
Route::get('/users/dashboard', 'UsersController@dashboard');
Route::get('/users/verify/{id}', 'UsersController@verify');
Route::get('/users/mobile', 'UsersController@mobile');
Route::get('/users/pin', 'UsersController@pin');
Route::get('/users/locked-account', 'UsersController@locked');