<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Route as Route;
use Illuminate\Http\Request as Request;
use Illuminate\Http\Response as Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Util\SendEmail as UtilMailer;
use Session;
use Auth;
use Crypt;
use Hash;
use Cookie;
use App\User;

class UsersController extends Controller
{
    
    private $_user = null;
    private $_password = '';
    
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['login', 'register', 'verify']]);
    }
    
    public function dashboard() {
        
        if (Auth::user()->valid_pin === false) {
            return redirect('users/pin');
        }
        
        return view('users.dashboard');
    }
    
    /**
     * Register Route attempting to Register
     * new User by calling Class internal 
     * helper methods.
     * 
     * saving to users table.
     * @param Request $request
     */
    public function register(Request $request) {
        
        if ($request->ajax()) {
            
            // If user is saved then send verification email
            if ($this->addUser($request)) {
                return $this->sendVerificationEmail($request);
            } else {
                return 0;
            }
            
        } else {
            // redirect User back to Home if no Ajax
            return redirect('/');
        }
        
    }
    
    public function login(Request $request) {
        
        $auth = Auth::attempt(['username' => $request['username'], 'password' => $request['password']]);
        
        return ($auth) ? '1' : '0';
        
    }
    
    /**
     * Verfiy Route - Determines if User attempting to
     * register have recieved the verification email.
     * 
     * @param int $id
     * @return View
     */
    public function verify($secret) {
        
        $user = User::find(Crypt::decrypt($secret));
        
        if (isset($user)) {
            
            Auth::login($user);
            
            if (Auth::check()) {
                return redirect('/users/mobile');
            } else {
                return redirect('/');
            }
            
        } else {
            return redirect('/');
        }
        
        
        
    }
    
    /**
     * Redirectd Dsplay View which request User to insert
     * Mobile number to recieve pin.
     * 
     * @return type
     */
    public function mobile() {
        
        return view('users.mobile');
    }
    
    /**
     * SMS and Update User with the auto generated
     * pin.
     * 
     * @param Request $request
     * @return int
     */
    public function sendPin(Request $request) {
        
        // return 1/0 = function sendSMStoUser($mobile);
        
        if ($request->ajax()) {
            
            $this->_user            = User::findOrFail(Auth::user()->user_id);
            $this->_user->cell_no  = $request['mobile'];
            $this->_user->pin      = mt_rand(1000, 9999);
            
            if ($this->_user->save()) { 
                return '1';
            } else {
                return '0';
            }
            return '1';
            
        } else {
            return '0';
        }
        
    }
    
    /**
     * Validates if the User Pin exists and if 
     * pin exists then login user.
     * 
     * @param Request $request
     * @return string $indicator
     */
    public function checkPin(Request $request) {
        
        $user = Auth::user();
        
        if ($request->ajax()) {
            
            if ($user->pin === (int)$request['pin']) {
                
                if ($user->is_active === false) {
                    return 'locked';
                }
                
                $user->valid_pin = true;
                $user->attempts = 3;
                $user->save();
                
                return 'valid';
                
            } else {
                
                return $this->getLogOnAttempts($user);
            }
        }

    }
    
    public function pin(Request $request) {
        
        Auth::user()->valid_pin = false;
        Auth::user()->save();
        
        return view('users.pin');
        
    }
    
    public function locked() {
        
        Auth::logout();
        
        return view('users.locked_account');
        
    }

    /**
     * Add new User
     * 
     * @param Request $request
     * @return boolean
     */
    private function addUser($request) {
        
        $request['group_id']        = 2;
        $request['username']        = $request['email'];
        $request['token']           = $request['email'];
        $request['is_active']       = true;
        $request['valid_pin']       = false;
        $request['attempts']        = 3;
        
        $this->_user        = User::create($request->all());
        $this->_password    = $request['password'];
        
        if ($this->_user) {
            return true;
        } else {
            return false;
        }
        
    }
    
    private function getLogOnAttempts(User $user) {
        
        if ($user->is_active === false) {
            return 'locked';
        }
        
        if ($user->is_active && ($user->attempts === 0 || $user->attempts === NULL)) {
            
            $user->is_active = false;
            $user->attempts = 3;
            $user->save();
            return 'locked';
        } else {
            
            $attempt = $user->attempts - 1;
            $user->attempts = $attempt;
            $user->save();
            return 'invalid';
            
        }
    }
    
    /**
     * Send Verification Email to User
     * attempting to Register.
     * 
     * @param Request $request
     * @return int
     */
    private function sendVerificationEmail($request) {
        
        $url        = "users/verify";
        $secret     = Crypt::encrypt($this->_user->user_id);
        $uri        = url($url, $parameters = array($secret), $secure = false);
        $recipient  = ['name' => $request['name'], 'lastname' => $request['lastname'], 'email' => $request['email']];
        $data       = ['name' => $request['name'], 'lastname' => $request['lastname'], 'url' => $uri];
        $mailer     = new UtilMailer("verification", $recipient, $data);
        
        return $mailer->didSend;
    }
    
}
