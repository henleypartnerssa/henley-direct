<?php

namespace App\Util;

use Mail;

//use Illuminate\Mail\Mailer as Mailer;

class SendEmail {
    
    public      $didSend;
    
    /**
     * Class Constructor.
     * 
     * @param type $type
     * @param type $listOfRecipients
     * @param type $dataForEmail
     */
    public function __construct($type, $listOfRecipients = array() , $dataForEmail = array()) {
        
        if ($type === 'verification') {
            $this->sendVerficationEmail($listOfRecipients, $dataForEmail);
        }
        
    }
    
    
    /**
     * Sends User Account Verification email when
     * User attempts to register accout.
     * 
     * @param array $recipients
     * @param array $data
     */
    private function sendVerficationEmail($recipients, $data) {
        
        $this->didSend = Mail::send('email.verification', $data, function($msg) use ($recipients) {
         
            $msg->to($recipients['email'], $recipients['name'])->subject('Henley Direct - Account Verification');
            
        });
        
    }
    
}