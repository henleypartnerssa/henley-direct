<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    
    protected $table        = 'applicants';
    protected $primaryKey   = 'applicant_id';
    protected $fillable     = ['user_id',
                                'title',
                                'first_name',
                                'middle_name',
                                'Surname',
                                'other_names',
                                'former_names',
                                'first_given_names',
                                'place_of_birth',
                                'country_of_birth',
                                'date_of_birth',
                                'marital_status',
                                'gender',
                                'details_of_marital_status',
                                'citizenship_at_birth',
                                'citizenship_of_other_countries',
                                'identity_card_no',
                                'social_security_no',
                                'languages',
                                'reason_for_application',
                                'completing_form_as',
                                'perm_tel_no',
                                'mobile_no',
                                'email',
                                'occupation',
                                'self_employed',
                                'nature_of_business',
                                'important_business_dependancies',
                                'business_or_employer_name',
                                'business_or_emp_web_url',
                                'business_tel_no',
                                'business_email',
                                'net_income',
                                'source_of_income',
                                'gio_jur_business_activities',
                                'total_net_worth',
                                'gio_jur_business_tot_net_worth',
                                'total_net_worth_held_by',
                                'total_net_worth_summary',
                                'currency'];
    
    protected $hidden       = [''];
    
    
    
    
}
