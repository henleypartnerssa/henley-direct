$('document').ready(function() {
    
    // Declare Form
    var form = $('.ui.pin.form');
    // Declare Form Send Button
    var sendBtn = form.find('#checkPin');
    
    // On Send Pin Button Click
    sendBtn.on('click', function() {
        
        // validate form
        
        var token   = $('#_token').val();
        var num1    = $('#num1').val();
        var num2    = $('#num2').val();
        var num3    = $('#num3').val();
        var num4    = $('#num4').val();
        var number  = num1.concat(num2).concat(num3).concat(num4);
        // validate Pin via Ajax Call
        $.ajax({
            type: 'POST',
            url: '/users/mobile/pin',
            async: true,
            data: '_token='+ token +'&pin=' + Number(number),
            success: function(data, textStatus, jqXHR) {
                if (data === 'valid') {
                    window.location.href = '/users/dashboard';
                } else if (data === 'invalid') {
                    $('#sendButton').removeClass("loading");
                    $('#sendButton').text("resend pin");
                } else {
                    window.location.href = '/users/locked-account';
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
        
    });
    
});