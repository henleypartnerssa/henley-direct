$('document').ready(function() {
    
    $('.ui.dropdown').dropdown();
    
    $('.ui.coupled.modal').modal({
        allowMultiple: false
    });
    
});

signUpBtn();
loginBtn();
logonBtn();
registerButtom();

/**
 * Validates Registration form and
 * returns if Validation Passed or failed.
 * @returns bool
 */
function regFormValidation() {
    
    // Define Form Fields gto Validate
    var test = $('.ui.register.form').form({
        fields: {
            
            name: {
                identifier  : 'name',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your name'
                    }
                ]
            },
            lastname: {
                identifier  : 'lastname',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your Last Name'
                    }
                ]
            },
            password: {
                identifier  : 'register_password',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your password'
                    }
                ]
            },
            retypePassword: {
                identifier  : 'retypePassword',
                rules: [
                    {
                        type   : 'match[register_password]',
                        prompt : 'Password not mathcing'
                    }
                ]
            },
            email: {
                identifier  : 'email',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your email'
                    },
                    {
                        type   : 'email',
                        prompt : 'Please enter valid email address'
                    }
                ]
            },
            retypeEmail: {
                identifier  : 'retypeEmail',
                rules: [
                    {
                        type   : 'match[email]',
                        prompt : 'Email not matching'
                    },
                    {
                        type   : 'email',
                        prompt : 'Please enter valid email address'
                    }
                ]
            } 
        },
        inline : true,
        on     : 'blur'
    });
    
    return $('.ui.register.form').form('is valid');
}

function loginFormValidation() {
    
    // Define Form Fields gto Validate
    var test = $('.ui.login.form').form({
        fields: {
            usename: {
                identifier  : 'username',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your username'
                    },
                    {
                        type   : 'email',
                        prompt : 'Please enter valid email address'
                    }
                ]
            },
            password: {
                identifier  : 'login_password',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your password'
                    }
                ]
            }
        },
        inline : true,
        on     : 'blur'
    });
    
    return $('.ui.login.form').form('is valid');
}

function getFormData() {
    
    var form    = $('.ui.large.form');
    
    var token           = form.find('#_token').val();
    var name            = form.find('#name').val();
    var lastName        = form.find('#lastname').val();
    var password        = form.find('#register_password').val();
    var retypePassword  = form.find('#retypePassword').val();
    var email           = form.find('#email').val();
    var retypeEmail     = form.find('#retypeEmail').val();
    
    var data = "_token="+token+"&name="+name+"&lastname="+lastName+"&password="+encodeURIComponent(password)+"&email="+email;
    
    return data;
}

function signUpBtn() {
    $('.ui.signup.button').on('click', function(){
        $('.first.modal').modal('show');
    });
}

function loginBtn() {
    $('.ui.login.button').on('click', function() {
        $('.ui.login.modal').modal('show');
    });
}

function logonBtn() {
    
    $('.ui.logon.submit').on('click', function() {
        
        var val = loginFormValidation();
        
        var getUsername = $('#username').val();
        var getPassword = $('#login_password').val();
        var token       = $('#login_token').val();
        
        if (val === true) {
            $.ajax({

                type: 'POST',
                url:'/users/login',
                async: true,
                data: '_token='+token+'&username='+getUsername+'&password='+encodeURIComponent(getPassword),
                success : function(data, textStatus, jqXHR) {
                    if (data === '1') {
                        window.location.href = '/users/pin';
                    } else {
                        showLogInError("Incorrect Username or Password. Please Try again.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }

            });
        }
        
    });
}

function registerButtom() {
    $('.ui.register.submit').on('click', function() {
        
        var val = regFormValidation();
        
        // If form Validates then
        // create user and Oprn 2nd
        // modal view.
        if (val === true) {
            
            $.ajax({
                
                type: 'POST',
                url:'/users/register',
                async: true,
                data: getFormData(),
                success : function(data, textStatus, jqXHR) {
                    $('span#echoEmail').text($('.ui.large.form').find("#email").val());
                    $('.second.modal').modal('show');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
                
            });
            
        }
    });
}

function showLogInError(message) {
    
    var msgBox = $('.ui.login.message');
    msgBox.find('.header').text("Invalid Login Details");
    msgBox.find('p').text(message);
    msgBox.show().delay(3500).fadeOut();;
    
}